<?php
use mgrechanik\yiimaterializedpath\ServiceInterface;
use app\models\Animal;
use mgrechanik\yiimaterializedpath\widgets\TreeToListWidget;
/* @var $this yii\web\View */

$service = \Yii::createObject(ServiceInterface::class);
?>
<h1>animal/index</h1>

<p>
    You may change the content of this page by modifying
    the file <code><?= __FILE__; ?></code>.
</p>
<?php
$root = $service->getRoot(Animal::class);
$tree = $service->buildDescendantsTree($root);
print TreeToListWidget::widget(['tree' => $tree]);