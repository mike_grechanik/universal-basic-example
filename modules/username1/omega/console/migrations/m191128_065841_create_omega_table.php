<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%omega}}`.
 */
class m191128_065841_create_omega_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%omega}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%omega}}');
    }
}
