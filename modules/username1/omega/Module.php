<?php

namespace modules\username1\omega;

use mgrechanik\yiiuniversalmodule\UniversalModule;

class Module extends UniversalModule
{
    public $backendControllers = [
        'admin-default'
    ];
}
