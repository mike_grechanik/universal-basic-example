<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model modules\username1\omega\models\Omega */

$this->title = 'Update Omega: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Omegas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="omega-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
