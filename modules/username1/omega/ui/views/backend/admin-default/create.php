<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model modules\username1\omega\models\Omega */

$this->title = 'Create Omega';
$this->params['breadcrumbs'][] = ['label' => 'Omegas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="omega-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
