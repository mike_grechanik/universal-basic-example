<?php
namespace modules\example;

use mgrechanik\yiiuniversalmodule\UniversalModule;

class Module extends UniversalModule
{
    public $frontendControllers = [
        'default',
        'default2',
    ];

    public $backendControllers = [
        'admin-default'
    ];
}