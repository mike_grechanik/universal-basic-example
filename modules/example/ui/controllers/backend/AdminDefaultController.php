<?php

namespace modules\example\ui\controllers\backend;

use yii\web\Controller;

/**
 * Default controller for the `example` module
 */
class AdminDefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
