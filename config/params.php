<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'backendLayout' => '//lte/main',
    // 
    'backendControllerConfig' => [
        /*
        // можно и через базовую аутентификацию вводить свои данные
        'as basicaccess' => [
            'class' => yii\filters\auth\HttpBasicAuth::class,
            'auth' => function ($username, $password) {
                $user = app\models\User::findByUsername($username);
                if ($user && $user->validatePassword($password)) {
                    return $user;
                }
                return null;
            },
        ], */       
        'as backendaccess' => [
            'class' => \yii\filters\AccessControl::class,
            'rules' => [
                [
                    'allow' => true,
                    'ips' => ['127.0.0.1'],
                    'matchCallback' => function ($rule, $action){
                        $user = \Yii::$app->user;
                        return !$user->isGuest &&
                            ($user->id == 100);
                },
                ]
            ],
        ],
    ],	
  
];
