<?php

namespace app\models;

use Yii;
use mgrechanik\yiimaterializedpath\MaterializedPathBehavior;
/**
 * This is the model class for table "animal".
 *
 * @property int $id
 * @property string $path Path to parent node
 * @property int $level Level of the node in the tree
 * @property int $weight Weight among siblings
 * @property string $name Name
 */
class Animal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'animal';
    }
	
    public function behaviors()
    {
        return [
            'materializedpath' => [
                'class' => MaterializedPathBehavior::class,
				'modelScenarioForChildrenNodesWhenTheyDeletedAfterParent' => 'SCENARIO_NOT_DEFAULT',
            ],
        ];
    } 	

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['level', 'weight'], 'integer'],
            [['name'], 'required'],
            [['path', 'name'], 'string', 'max' => 255],
        ];
    }
	
    /**
     * {@inheritdoc}
     */
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_DELETE,
        ];
    }	

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'path' => 'Path',
            'level' => 'Level',
            'weight' => 'Weight',
            'name' => 'Name',
        ];
    }
}
