<?php

namespace app\controllers;

use app\models\Animal;
use mgrechanik\yiimaterializedpath\ServiceInterface;

class AnimalController extends \yii\web\Controller
{
	private $service;
	
	public function init()
	{
		parent::init();
		$this->service = \Yii::createObject(ServiceInterface::class);
	}
    public function actionIndex()
    {
        return $this->render('index');
    }
	
    public function actionCheck1()
    {
		$root = $this->service->getRoot(Animal::class);
		$root->add(new Animal(['name' => 'new2']));		
        return $this->render('check1');
    }	
	
    public function actionCheck2()
    {
		$model1 = Animal::findOne(1);
		$model1->add(new Animal(['name' => 'new3']));		
        return $this->render('check1');
    }	

}
